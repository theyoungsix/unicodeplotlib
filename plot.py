#!/usr/bin/env python3

import numpy as np
from random import uniform
from random import gauss

class Plot:
    def __init__(self, x, y, marker='•', line=False, xmin=None, xmax=None, ymin=None, ymax=None, xstep=1.0,
                 ystep=1.0, title=None, xtitle=None, ytitle=None):
        if xmin is None:
            xmin = min(x)
        if xmax is None:
            xmax = max(x)
        if ymin is None:
            ymin = min(y)
        if ymax is None:
            ymax = max(y)
        self.title = title
        self.xtitle = xtitle
        self.ytitle = ytitle
        self.xaxis = Axis(lim=(xmin, xmax), step=float(xstep))
        self.yaxis = Axis(lim=(ymin, ymax), step=float(ystep))
        self.marker = marker
        self.line = line
        self.data = []
        # fill data with matching datasets. use the length of the smaller
        for i in range(min(len(x), len(y))):
            self.data.append((x[i], y[i]))
        self.data.sort(key=lambda p: p[0])

    # this is complicated logic with displaying grids and tics and axes. I don't even understand it.
    def show(self, show_xlab=False, show_ylab=True, show_xtics=False, show_ytics=False, xdigs=None,
             ydigs=None, show_xgrid=False, show_ygrid=False, line=True):
        xrange = self.xaxis.lab
        yrange = list(reversed(self.yaxis.lab))
        xlab_len = self.xaxis.long_lab_len(xdigs)
        ylab_len = self.yaxis.long_lab_len(ydigs)
        disp_xlab = self.xaxis.get_disp_lab(digs=xdigs)
        disp_ylab = self.yaxis.get_disp_lab(digs=ydigs)

        draw_yaxis = min(xrange)-self.xaxis.step/2 <= 0 and max(xrange)+self.xaxis.step/2 >= 0

        if not self.ytitle is None:
            ytitle_col = self.ytitle.center(len(yrange))
            ytitle_row_len = 2
        else:
            ytitle_row_len = 0

        if not self.title is None:
            title_row = self.title.center(len(xrange)+2)
            print(title_row.rjust(ytitle_row_len + ylab_len + len(title_row)))

        print(f"{''.rjust(ytitle_row_len + ylab_len)}┌{'─'*len(xrange)}┐")
        for y in yrange:
            ymax = y + self.yaxis.step/2
            ymin = y - self.yaxis.step/2
            draw_xaxis = ymax >= 0 and ymin <= 0

            if draw_xaxis or (show_ygrid and disp_ylab[yrange.index(y)] != ' '):
                xchar = '─'
                yax_char = '┼'
            else:
                xchar = ' '
                if (show_ytics or show_ygrid) and disp_ylab[yrange.index(y)] != ' ':
                    yax_char = '┼'
                else:
                    yax_char = '│'

            if show_ylab and disp_ylab[yrange.index(y)] != ' ':
                left_border = '├'
            else:
                left_border = '│'
            out = [left_border] + [xchar]*len(xrange) + ['│']
            if show_xgrid or show_ygrid:
                if show_xgrid and not show_ygrid and not draw_xaxis:
                    gridchar = '│'
                elif show_ygrid and not show_xgrid:
                    gridchar = '─'
                else: # show xgrid and ygrid
                    gridchar = '┼'
                for i in range(len(disp_xlab)):
                    if disp_xlab[i] != ' ':
                        out[i+1] = gridchar
            if draw_xaxis and show_xtics:
                for i in range(len(disp_xlab)):
                    if disp_xlab[i] != ' ':
                        out[i+1] = '┼'
            if draw_yaxis:
                xmin = -self.xaxis.step/2
                xmax = self.xaxis.step/2
                for x in xrange:
                    if x >= xmin and x <= xmax:
                        out[xrange.index(x)+1] = yax_char
                        break

            for point in self.data:
                if point[1] <= ymax and point[1] >= ymin:
                    for x in xrange:
                        xmin = x - self.xaxis.step/2
                        xmax = x + self.xaxis.step/2
                        if point[0] >= xmin and point[0] <= xmax:
                            if self.line:
                                index = self.data.index(point)
                                if index-1 < 0:
                                    slope_prev = None
                                else:
                                    prev_point = self.data[index-1]
                                    slope_prev = (point[1] - prev_point[1])/(point[0] - prev_point[0])
                                if index+1 == len(self.data):
                                    slope_next = None
                                else:
                                    next_point = self.data[index+1]
                                    slope_next = (next_point[1] - point[1])/(next_point[0] - point[0])
                                # average the slopes
                                if slope_prev is None and slope_next is None:
                                    marker = '·'
                                elif slope_prev is None:
                                    slope = slope_next
                                elif slope_next is None:
                                    slope = slope_prev
                                else: # neither is None
                                    slope = 0.5*(slope_prev + slope_next)
                                if slope <= -2:
                                    marker = '|'
                                elif slope <= -0.5:
                                    marker = '\\'
                                elif slope <= 0.5:
                                    marker = '-'
                                elif slope <= 2:
                                    marker = '/'
                                else: # slope > 2
                                    marker = '|'
                            else:
                                marker = self.marker
                            out[xrange.index(x)+1] = marker

            if self.ytitle is None:
                ytitle_row = ''
            else:
                ytitle_row = ytitle_col[yrange.index(y)] + ' '
            if show_ylab:
                lab = disp_ylab[self.yaxis.lab.index(y)].rjust(ylab_len)
            else:
                lab = ''
            print(ytitle_row + lab + ''.join(out))
        bottom_border = ['└'] + ['─']*len(xrange) + ['┘']
        if show_xlab:
            for i in range(len(disp_xlab)):
                if disp_xlab[i] != ' ':
                    bottom_border[i+1] = '┴'
        print(f"{''.rjust(ytitle_row_len + ylab_len)}{''.join(bottom_border)}")
        if show_xlab:
            for r in range(xlab_len):
                out = ''.rjust(ytitle_row_len + ylab_len+1)
                for x in disp_xlab:
                    out += x.ljust(xlab_len)[r]
                print(out)
        if not self.xtitle is None:
            title_row = self.xtitle.center(len(xrange)+2)
            if show_xlab:
                print()
            print(title_row.rjust(ytitle_row_len + ylab_len + len(title_row)))

class Axis:
    def __init__(self, lim=(-10,10), step=1.0, title=None):
        self.step = step
        self.lim = lim
        self.lab = list(np.arange(lim[0], lim[1]+step, step))
        self.title = title

    def long_lab_len(self, digs=None):
        length = 0
        for lab in self.lab:
            if digs is None:
                disp_lab = str(lab).rstrip('0').rstrip('.')
            else:
                disp_lab = str(round(lab, digs)).rstrip('0').rstrip('.')
            if disp_lab == '-0':
                disp_lab = '0'
            lab_len = len(disp_lab)
            if lab_len > length:
                length = lab_len
        return length

    def get_disp_lab(self, digs=None):
        disp_lab = []
        for lab in self.lab:
            if digs is None:
                disp_lab.append(str(lab).rstrip('0').rstrip('.'))
            else:
                disp_lab.append(str(round(lab, digs)).rstrip('0').rstrip('.'))
            if disp_lab[-1] == '-0':
                disp_lab[-1] = '0'
        for i in range(len(disp_lab)):
            val = disp_lab[i]
            try:
                float(val)
            except ValueError:
                continue
            
            first_index = disp_lab.index(val)
            last_index = len(disp_lab) - list(reversed(disp_lab)).index(val)
            closest = None
            for j in range(first_index, last_index):
                diff = np.abs(self.lab[j] - float(val))
                if closest is None or diff < closest:
                    closest = diff
                    closest_index = j
            for j in range(first_index, last_index):
                if j != closest_index:
                    disp_lab[j] = ' '
        return disp_lab

## Examples
#
# x = np.linspace(-5,10,500)
# y = [0.5*np.exp(-0.125*xi)*(np.cos(xi)+np.sin(2*xi)) for xi in x]
# Plot(x, y, ystep=0.07, xstep=0.15, line=True, title="e⁻ᶿᐟ⁸(cos(θ) + sin(2θ))", ytitle="ODE Solution", xtitle='θ'
#      ).show(xdigs=0, ydigs=1, show_ytics=True, show_xlab=True, show_xtics=True, show_xgrid=False, show_ygrid=False)
#
# x = np.linspace(1,200,200)
# y = [-65*np.exp(-0.07*(xi-8))*np.cos(0.1*(xi-8)) + 250 - 50*(1-np.exp(-0.005*xi)) + 15*(np.exp(0.0005*xi)-1) + gauss(0,5) for xi in x]
# Plot(x, y, ystep=3.5, xstep=max(x)/100, marker='·', title="Validation Loss", ytitle="Mean Squared Error", xtitle='Epoch'
#      ).show(xdigs=0, ydigs=0, show_xlab=True, show_ytics=True, show_xtics=True, show_xgrid=False,
#             show_ygrid=False)
#
# x = [uniform(-50, 50) for _ in range(100)]
# y = [gauss(0, 5) for _ in x]
# Plot(x, y).show(ydigs=1)
