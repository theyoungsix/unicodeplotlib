#!/usr/bin/env python3

import shutil
import os
import platform

base_dir = os.path.dirname('{}'.format(os.getcwd()))
module_name = 'unicodeplotlib'

if 'Windows' in platform.platform():
    path = 'C:\\Python39\\lib\\' # windows for python 3.9
elif 'Linux' in platform.platform():
    path = '/usr/lib/python3.8/' # for linux python 3.8
else: # mac for python3.7
    path = '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/'

shutil.rmtree('{}{}'.format(path, module_name), ignore_errors=True)

shutil.copytree(
    '{}/unicodeplotlib/'.format(base_dir),
    '{}{}'.format(path, module_name)
)
